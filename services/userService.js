const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll() {
        return UserRepository.getAll();
    }

    create(data) {
        return UserRepository.create(data);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    searchById(id) {
        return this.search({ id });
    }

    updateById(id, dataToUpdate) {
        return UserRepository.update(id, dataToUpdate);
    }

    deleteById(id) {
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();
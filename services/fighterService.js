const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getAll() {
        return FighterRepository.getAll();
    }

    create(data) {
        return FighterRepository.create(data);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    searchById(id) {
        return this.search({ id });
    }

    updateById(id, dataToUpdate) {
        return FighterRepository.update(id, dataToUpdate);
    }

    deleteById(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();
const { Router } = require('express');
const FighterService = require('../services/fighterService');
const middleware = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (request, response, next) => {
    try {
        const allFighters = FighterService.getAll();
        response.json(allFighters);
    } catch (error) {
        response.err = error;
    } finally {
        next();
    }
});

router.get('/:id', middleware.getFighterValid);
router.post('/', middleware.createFighterValid);
router.put('/:id', middleware.updateFighterValid);
router.delete('/:id',middleware.deleteFighterValid);

router.use(middleware.fighterErrorHandler);

module.exports = router;
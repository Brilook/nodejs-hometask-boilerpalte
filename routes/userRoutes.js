const { Router } = require('express');
const UserService = require('../services/userService');
const middleware = require('../middlewares/user.validation.middleware');
const router = Router();

router.get('/', (request, response, next) => {
    try {
        const allUsers = UserService.getAll();
        response.json(allUsers);
    } catch (error) {
        response.err = error;
    } finally {
        next();
    }
});

router.get('/:id', middleware.getUserValid);
router.post('/', middleware.createUserValid);
router.put('/:id', middleware.updateUserValid);
router.delete('/:id',middleware.deleteUserValid);

// Should be the last one
router.use(middleware.userErrorHandler);

module.exports = router;
const { fighter: fighterModel } = require('../models/fighter');
const FighterService = require('../services/fighterService');


function checkIdIsMissing(request, response, next) {
    const dataToValidate = request.body;
    if (dataToValidate.id) {
        throw new Error('Id parameter is excess.');
    } else {
        next();
    }
}

function eqSet(as, bs) {
    if (as.size !== bs.size) return false;
    for (var a of as) if (!bs.has(a)) return false;
    return true;
}

function checkAllProperiesArePresent(request, response, next) {
    const set1 = new Set(Object.getOwnPropertyNames(fighterModel));
    set1.delete('id');
    const set2 = new Set(Object.getOwnPropertyNames(request.body));
    if (!eqSet(set1, set2)) {
        throw new Error(`Bad props`);
    } else {
        next();
    }
}

function checkPowerValid(request, response, next) {
    const dataToValidate = request.body;
    if (typeof dataToValidate.power !== 'number' || dataToValidate.power > 100 || dataToValidate.power < 0) {
        throw new Error('The power should be < 100!');
    } else {
        next();
    }
}


function storeFighter(request, response, next) {
    const dataToValidate = request.body;
    let dataToStore = {};

    for (let property in fighterModel) {
        if (property !== 'id') {
            dataToStore[property] = dataToValidate[property];
        }
    }

    const newFighter = FighterService.create(dataToStore);
    response.json(newFighter);
}

function checkFighterExistsWithIdFromParam(request, response, next) {
    const id = request.params.id;
    if (!FighterService.searchById(id)) {
        let error = new Error(`User not found.`);
        error.statusCode = 404;
        throw error;
    } else {
        next();
    }
}

function getFighter(request, response, next) {
    const id = request.params.id;
    const fighter = FighterService.searchById(id);
    response.json(fighter);
}

function updateFighter (request, response, next) {
    const dataToValidate = request.body;
    const id = request.params.id;
    let dataToUpdate = {};

    for (let property in fighterModel) {
        if (property !== 'id') {
            dataToUpdate[property] = dataToValidate[property];
        }
    }

    const updateFighter = FighterService.updateById(id, dataToUpdate);
    response.json(updateFighter);
}

function deleteFighter (request, response, next) {
    const id = request.params.id;
    FighterService.deleteById(id);
    response.json({
        error: false,
        message: 'OK'
    });
}

const createFighterValid = [
    checkIdIsMissing,
    checkAllProperiesArePresent,
    checkPowerValid,
    storeFighter
];

const getFighterValid = [
    checkFighterExistsWithIdFromParam,
    getFighter
];

const updateFighterValid = [
    checkIdIsMissing,
    checkFighterExistsWithIdFromParam,
    checkAllProperiesArePresent,
    checkPowerValid,
    updateFighter
];

const deleteFighterValid = [
    checkFighterExistsWithIdFromParam,
    deleteFighter
];

const fighterErrorHandler = (error, request, response, next) => {
    const responseBody = {
        error: true,
        message: error.message
    }
    
    response.status(error.statusCode || 400).json(responseBody);
};

exports.getFighterValid = getFighterValid;
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.deleteFighterValid = deleteFighterValid;
exports.fighterErrorHandler = fighterErrorHandler;
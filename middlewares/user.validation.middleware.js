const { user: userModel } = require('../models/user');
const UserService = require('../services/userService');

function checkIdIsMissing(request, response, next) {
    const dataToValidate = request.body;
    if (dataToValidate.id) {
        throw new Error('Id parameter is excess.');
    } else {
        next();
    }
}
function eqSet(as, bs) {
    if (as.size !== bs.size) return false;
    for (var a of as) if (!bs.has(a)) return false;
    return true;
}

function checkAllProperiesArePresent(request, response, next) {
    const set1 = new Set(Object.getOwnPropertyNames(userModel));
    set1.delete('id');
    const set2 = new Set(Object.getOwnPropertyNames(request.body));
    if (!eqSet(set1, set2)) {
        throw new Error(`Bad props`);
    } else {
        next();
    }
}

function checkEmailIsValid(request, response, next) {
    const dataToValidate = request.body;
    if (dataToValidate.email.indexOf('@gmail') === -1){
        throw new Error(`Wrong email!`);
    }
    next();
}

function checkPhoneIsValid(request, response, next) {
    const dataToValidate = request.body;
    if (dataToValidate.phoneNumber.slice(0,4) !== '+380' ){
        throw new Error(`Wrong phone number!`);
    }
    next();
}

function checkPasswordIsValid(request, response, next) {
    const dataToValidate = request.body;
    if (dataToValidate.password.length < 3) {
        throw new Error(`Password is too short.`);
    } else {
        next();
    }
}

function checkUserExistsWithIdFromParam(request, response, next) {
    const id = request.params.id;
    if (!UserService.searchById(id)) {
        let error = new Error(`User not found.`);
        error.statusCode = 404;
        throw error;
    } else {
        next();
    }
}

function getUser(request, response, next) {
    const id = request.params.id;
    const user = UserService.searchById(id);
    response.json(user);
}

function storeUser(request, response, next) {
    const dataToValidate = request.body;
    let dataToStore = {};

    for (let property in userModel) {
        if (property !== 'id') {
            dataToStore[property] = dataToValidate[property];
        }
    }

    const newUser = UserService.create(dataToStore);
    response.json(newUser);
}

function updateUser(request, response, next) {
    const dataToValidate = request.body;
    const id = request.params.id;
    let dataToUpdate = {};

    for (let property in userModel) {
        if (property !== 'id') {
            dataToUpdate[property] = dataToValidate[property];
        }
    }

    const updatedUser = UserService.updateById(id, dataToUpdate);
    response.json(updatedUser);
}

function deleteUser(request, response, next) {
    const id = request.params.id;
    UserService.deleteById(id);
    response.json({
        error: false,
        message: 'OK'
    });
}

const getUserValid = [
    checkUserExistsWithIdFromParam,
    getUser
];

const createUserValid = [
    checkIdIsMissing,
    checkAllProperiesArePresent,
    checkEmailIsValid,
    checkPhoneIsValid,
    checkPasswordIsValid,
    storeUser
];

const updateUserValid = [
    checkIdIsMissing,
    checkUserExistsWithIdFromParam,
    checkAllProperiesArePresent,
    checkEmailIsValid,
    checkPhoneIsValid,
    checkPasswordIsValid,
    updateUser
];

const deleteUserValid = [
    checkUserExistsWithIdFromParam,
    deleteUser
];

const userErrorHandler = (error, request, response, next) => {
    const responseBody = {
        error: true,
        message: error.message
    }
    
    response.status(error.statusCode || 400).json(responseBody);
};
  
exports.getUserValid = getUserValid;
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.deleteUserValid = deleteUserValid;
exports.userErrorHandler = userErrorHandler;
